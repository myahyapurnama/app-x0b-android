package purnama.yahya.appx0b

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*

class AdapterDataMasker(val dataMasker: List<HashMap<String,String>>,
                        val mainActivity: MainActivity) :
    RecyclerView.Adapter<AdapterDataMasker.HolderDataMasker>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataMasker.HolderDataMasker {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_data,p0,false)
        return HolderDataMasker(v)
    }

    override fun getItemCount(): Int {
        return dataMasker.size
    }

    override fun onBindViewHolder(p0: AdapterDataMasker.HolderDataMasker, p1: Int) {
        val data = dataMasker.get(p1)
        p0.txIdMasker.setText(data.get("id_masker"))
        p0.txNama.setText(data.get("nama"))
        p0.txWarna.setText(data.get("warna"))
        p0.txTipeWajah.setText(data.get("tipe_wajah"))

        //beginNew
        if(p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        p0.cLayout.setOnClickListener(View.OnClickListener {
            val pos = mainActivity.daftarTipeWajah.indexOf(data.get("tipe_wajah"))
            mainActivity.spTipeWajah.setSelection(pos)
            mainActivity.edIdMasker.setText(data.get("id_masker"))
            mainActivity.edNama.setText(data.get("nama"))
            mainActivity.edWarna.setText(data.get("warna"))
            Picasso.get().load(data.get("url")).into(mainActivity.imUP)
        })
        //endNew
        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo);
    }

    class HolderDataMasker(v: View) : RecyclerView.ViewHolder(v){
        val txIdMasker = v.findViewById<TextView>(R.id.txIdMasker)
        val txNama = v.findViewById<TextView>(R.id.txNama)
        val txWarna = v.findViewById<TextView>(R.id.txWarna)
        val txTipeWajah = v.findViewById<TextView>(R.id.txTipeWajah)
        val photo = v.findViewById<ImageView>(R.id.imPhoto)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }
}